node default {
	package { [ 'httpd', 'php', 'mysql-server' ] : ensure => installed, }
	service { [ 'httpd', 'mysqld' ] : ensure => running, }
}
